/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package instituto;

import java.util.LinkedList;

/**
 *
 * @author nachorod
 */
public final class Alumno extends Persona {

    private double notaFinal;
    //private Grupo g;
    //LinkedList<Asignatura> asignaturas;
    
    public Alumno(String nombre, int edad) {
        super(nombre, edad);
    }

    public double getNotaFinal() {
        return notaFinal;
    }

    public void setNotaFinal(double notaFinal) {
        this.notaFinal = notaFinal;
    }

    @Override
    public String toString() {
        return "Alumno{" + "Nombre=" + super.getNombre() + " Edad=" + super.getEdad() + " notaFinal=" + notaFinal + '}';
    }
    
    
    
    
}
