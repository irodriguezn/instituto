/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package instituto;

import java.util.LinkedList;

/**
 *
 * @author admin
 */
public class Contenedor {
    LinkedList<Asignatura> asignaturas = new LinkedList<>();
    LinkedList<Profesor> profesores= new LinkedList<>(); 
    LinkedList<Alumno> alumnos= new LinkedList<>(); 
    //LinkedList<Grupo> grupos= new LinkedList<>(); 
    
    public void addAsignatura(Asignatura as) {
        asignaturas.add(as);
    }
    
    public void addProfesor(Profesor p) {
        profesores.add(p);
    }
    
    public void addAlumno(Alumno a) {
        alumnos.add(a);
    }
    
    public void listarAsignaturas() {
        System.out.println(asignaturas);
    }
        

}
