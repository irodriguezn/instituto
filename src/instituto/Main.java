package instituto;

import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author nachorod
 */
public class Main {
    
    public static void main(String[] args) {
        Contenedor c= new Contenedor();
        
        int op=5;
        while(op!=0) {
            op=menuPrincipal();
            gestionaPrincipal(op, c);
        }
    }
    
    private static int menuPrincipal() {
        int op=0;
        System.out.println("\nMenú Principal");
        System.out.println("---------------");
        System.out.println("1- Asignaturas");
        System.out.println("2- Profesores");
        System.out.println("3- Alumnos");
        System.out.println("4- Grupos");
        System.out.println("0- Salir");
        return Util.validaEntero("Introduzca una opción (0-4): ", 0, 4);
    }
    
    private static void gestionaPrincipal(int op, Contenedor c) {
        String op2="";
        switch (op) {
            case 1:
                while (!op2.equalsIgnoreCase("s")) {
                    op2=menuCrud("Asignaturas");
                    gestionaAsignaturas(op2, c);
                }
                break;
            case 2:
                while (!op2.equalsIgnoreCase("s")) {
                    op2=menuCrud("Profesores");
                    gestionaProfesores(op2, c);
                }
                break;
            case 3:
                while (!op2.equalsIgnoreCase("s")) {
                    op2=menuCrud("Alumnos");
                    gestionaAlumnos(op2, c);
                }
                break;
            case 4:
                while (!op2.equalsIgnoreCase("s")) {
                    op2=menuCrud("Grupos");
                    gestionaGrupos(op2, c);                
                }
                break;
        }
    }
   
    private static String menuCrud(String titulo) {
        System.out.println("\n"+titulo);
        Util.repiteCaracter("-", titulo.length());
        System.out.println("\nc- Introducir ");
        System.out.println("r- Listar ");
        System.out.println("u- Actualizar ");
        System.out.println("d- Borrar ");
        System.out.println("s- Salir");
        return Util.validaCrud("Introduzca una opción: ");
    }
    
    private static void gestionaAsignaturas(String op, Contenedor c) {
        switch (op) {
            case "c": 
                añadeAsignatura(c);
                break;
            case "r": 
                listaAsignaturas(c);
                break;            
            case "u":
                actualizaAsignatura(c);
                break;
            case "d":
                borraAsignatura(c);
                
        }
    }
    
    private static void gestionaProfesores(String op, Contenedor c) {
        System.out.println("Profesores");
    }

    private static void gestionaAlumnos(String op, Contenedor c) {
        System.out.println("Alumnos");
    }

    private static void gestionaGrupos(String op, Contenedor c) {
        System.out.println("Grupos");
    }    
    
    
    
    // ASIGNATURAS
    // -----------
    static void añadeAsignatura(Contenedor c) {
        String nombreAsignatura=Util.validaCadena("\nIntroduzca nombre: ", 50);
        int horas=Util.validaEntero("Número de horas: ", 1, 260);
        Asignatura as1=new Asignatura(nombreAsignatura, horas);
        c.addAsignatura(as1);
    }
    
    static void listaAsignaturas(Contenedor c) {
        System.out.println("\nListado Asignaturas");
        System.out.println("-------------------");
        c.listarAsignaturas();
    }
    
    static void actualizaAsignatura(Contenedor c) {
        
    }
    
    static void borraAsignatura(Contenedor c) {
        
    }
}
