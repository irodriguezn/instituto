package instituto;

/**
 *
 * @author nachorod
 */
public abstract class Persona {
    private String idPersona;
    private String nombre;
    private int edad;
    private static int numPersonas=0;

    public Persona(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
        this.idPersona=(numPersonas+1)+"";
        numPersonas++;
    }

    public static int getNumPersonas() {
        return numPersonas;
    }

    public String getIdPersona() {
        return idPersona;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", edad=" + edad + '}';
    }
    
    
}
