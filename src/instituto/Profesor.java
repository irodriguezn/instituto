
package instituto;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author nachorod
 */
public class Profesor extends Persona implements Funcionario {
    
    private double sueldo;
    private boolean tutor;
    LinkedList<Asignatura> asignaturas;
    
    public Profesor(String nombre, int edad) {
        super(nombre, edad);
    }

    @Override
    public void calculaNomina() {
        sueldo=1000;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public boolean isTutor() {
        return tutor;
    }

    public void setTutor(boolean tutor) {
        this.tutor = tutor;
    }

    public void addAsignatura(Asignatura as) {
        asignaturas.add(as);
    }
    
    public void removeAsignatura(String nombre) {
        Asignatura as;
        Iterator it=asignaturas.iterator();
        boolean noEncontrado=true;
        while (it.hasNext() && noEncontrado) {
            as=(Asignatura)it.next();
            if (as.getNombre().equals(nombre)) {
                noEncontrado=false;
                asignaturas.remove(as);
            }
        }
    }
    
    public void listarAsignaturas() {
        for (int i=0; i<asignaturas.size(); i++) {
            System.out.println(asignaturas.get(i).toString());
        }
    }
    
    @Override
    public String toString() {
        return "Profesor{" + "sueldo=" + sueldo + ", tutor=" + tutor + '}';
    }
    
    
}
