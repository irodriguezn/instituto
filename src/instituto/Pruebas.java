
package instituto;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author nachorod
 */
public class Pruebas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int cProfesor=0;
        int cAlumno=0;
        Persona p;
        
        LinkedList<Persona> personas=new LinkedList<>();
        
        Alumno a1= new Alumno("pablo", 33);
        Profesor p1= new Profesor("Nacho", 25);
        personas.add(a1);
        personas.add(p1);
        personas.add(new Profesor("Jordi", 37));
        
        System.out.println(Persona.getNumPersonas());
        
        Iterator it=personas.iterator();
        while (it.hasNext()) {
            p=(Persona)it.next();
            if (p instanceof Alumno) {
                cAlumno++;
            } else {
                cProfesor++;
            }
        }
        
        System.out.println("Núm Profesores: "+ cProfesor);
        System.out.println("Núm Alumnos: "+ cAlumno);
        System.out.println(a1.toString());
        
    }
    
}
