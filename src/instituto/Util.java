
package instituto;

import java.util.Scanner;

/**
 *
 * @author nacho
 */
public class Util {
    
    public static int validaEntero (String msj, int inicio, int fin) {
        int num=0;
        boolean salir=false;
        Scanner sc=new Scanner(System.in);
        while (!salir) {
            System.out.print(msj);
            try {
                num=Integer.parseInt(sc.nextLine());
                if (num>=inicio && num<=fin ) {
                    salir=true;
                } else {
                    System.out.println("El número ha de estar comprendido entre " + inicio + " y " + fin);
                }
            } catch (Exception e) {
                System.out.println("Caracter no numérico");
            }
            
        }
        return num;
    }

    public static String validaCrud (String msj) {
        String letrasOk="cruds";
        String op="";
        boolean salir=false;
        Scanner sc=new Scanner(System.in);
        while (!salir) {
            System.out.print(msj);
            op=sc.nextLine().toLowerCase();
            if (letrasOk.indexOf(op)==-1) {
                System.out.println("La opción no es correcta");
            } else {
                salir=true;
            }
        }
        return op;
    }    
    
    public static String validaCadena (String msj, int limite) {
        String op="";
        boolean salir=false;
        Scanner sc=new Scanner(System.in);
        while (!salir) {
            System.out.print(msj);
            op=sc.nextLine();
            if (op.length()>limite) {
                System.out.println("La cadena tiene que tener como máximo " + limite + " caracteres");
            } else {
                salir=true;
            }
        }
        return op;
    }    
    
    public static void repiteCaracter(String car, int numVeces) {
        for (int i=1; i<=numVeces; i++) {
            System.out.print(car);
        }
    }
}
